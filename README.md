# FacetPaint
This is a modernized fork of <https://bitbucket.org/shybovycha/moo3d>.

It is not known to load OBJ files from Blender yet.

Maintenance is pending a license (See the "Code" section below).

See [irrPaint3D](https://github.com/shybovycha/irrPaint3D) instead.
- 2021-03-31 I requested a license for that program at <https://stackoverflow.com/questions/8803917/realtime-object-painting>.


## Authors & License
### Code
- Authors: 2016 Artem Shoobovych ([shybovycha](https://bitbucket.org/shybovycha)), 2021 [Poikilos](https://bitbucket.org/poikilos)
- License: To be determined
  - 2021-03-31 I sent a request to connect to on LinkedIn with a note asking to add a license to his repo.

### Media
The assets from the old shybovycha/moo3d repo have been purged completely.

- Author: Jake "Poikilos" Gustafson
- License: Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)


## Compiling
```
sudo apt-get install libxmu-dev libxi-dev zlib1g freeglut3-dev
# zlib-bin
sudo apt-get install imagemagick libmagickwand-dev
# sudo apt-get install ruby-rmagick
# ^ formerly librmagick-ruby? See <https://superuser.com/questions/99898/trying-to-install-rmagick-on-debian>
sudo apt-get install ruby-bundler
sudo apt-get install ruby-dev
# ^ required for installing gems (See <https://stackoverflow.com/questions/20559255/error-while-installing-json-gem-mkmf-rb-cant-find-header-files-for-ruby>)
sudo apt-get install ruby-debug-inspector
sudo apt-get install pry

cd src
bundle install --path vendor/bundle

# not tried yet:
# libassimp-dev gem-plugin-assimp assimp-utils libassimp4 qt3d-assimpsceneimport-plugin
```

## Developer Notes
Debugger won't install (even `bundle update --all` breaks).
1. `bundle clean --dry-run` says:
"Could not find debugger-1.5.0 in any of the sources"
2. `bundle clean` says:
"Could not find debugger-1.5.0 in any of the sources"
3. `bundle remove debugger` says:
"`debugger` is not specified in /home/owner/git/irrpaint/src/Gemfile so it could not be removed."
4. `bundle remove pry-debugger`

# moo3d
[The author's original readme is below.]

This repository contains my graduation work; a simple 3D texture painting software.

The idea came from _[Cinema 4D](http://www.maxon.net/)_ and other softwares. The point is, i want to create a **very user-friendly** (unlike _[Blender](http://blender.org/)_) 3D modelling software with **free license** (unlike _[Cinema 4D](http://www.maxon.net/)_ and others) and **trivial process of object painting** (unlike _[3D Studio Max](http://usa.autodesk.com/3ds-max/)_). This repository is the starting point.

Many people may say _Why inventing a wheel?_. Still, many of them have written (for example) bubble sorting algorithm by their own hands. Just to learn something new. The same is here: i want to learn to write such kind of software. It's just fun for me, yet it is a serious project.

**So, what i ask is: be polite, respect yourself and my own; do not be a strict judge on what's happening in the repository, its wiki or source code. Just remember yourself at your student years. Thanks!**

## Dependencies

For so long as it is possible i shall push dependant libraries' sources to the repository.

For now, the software depends on:

* **openassimp**

## Building

[WIP]

* install the libraries required
    * for **Ubuntu Linux**:

            sudo apt-get install libxmu-dev libxi-dev zlib-bin freeglut3-dev
            sudo apt-get install imagemagick libmagickwand-dev
            sudo gem install rmagick

* build **openassimp**
      * for **Ubuntu Linux**:

            cd assimp
            sudo apt-get install cmake-qt-gui
            cmake-gui &>/dev/null &
            # then press "configure" and "generate" (if pressing "configure" did not end up with errors)

**Benchmarks:**

`
    2013-05-16T03:01:18+03:00 > Reading cube.obj file...
    2013-05-16T03:01:18+03:00 > OBJ file is done reading. Read 12 faces, 8 vertices.
    2013-05-16T03:01:18+03:00 > Triangulated OBJ geometry. Ended up with 36 triangles.
    2013-05-16T03:01:18+03:00 > Unwrapping done. Writing unwrapped texture to new_cube.png...
    2013-05-16T03:01:18+03:00 > Generated layers count: 1
    2013-05-16T03:01:19+03:00 > Unwrapping done. Writing unwrapped texture to old_cube.png...
    2013-05-16T03:01:19+03:00 > Generated layers count: 1
    2013-05-16T03:01:19+03:00 > Reading qt.obj file...
    2013-05-16T03:01:19+03:00 > OBJ file is done reading. Read 811 faces, 1644 vertices.
    2013-05-16T03:01:19+03:00 > Triangulated OBJ geometry. Ended up with 4866 triangles.
    2013-05-16T03:05:24+03:00 > Unwrapping done. Writing unwrapped texture to qt.png...
    2013-05-16T03:05:44+03:00 > Generated layers count: 16
`
