require 'moo_math'
require 'rmagick'

require 'pry'

# $DEBUG_DRAW = true

module MooMesh
    class Vertex < Vector
        attr_accessor :u, :v, :index, :copy_of

        def initialize(_x = 0.0, _y = 0.0, _z = 0.0, _u = nil, _v = nil, _index = nil)
            @x, @y, @z = _x.to_f, _y.to_f, _z.to_f
            @u, @v, @index = _u, _v, _index.to_i
            @x = 0.0 if @x =~ Numeric::ETHA
            @y = 0.0 if @y =~ Numeric::ETHA
            @z = 0.0 if @z =~ Numeric::ETHA
        end

        def to_s
            if @u.nil? or @v.nil?
                "<#{ @x }, #{ @y }, #{ @z }"
            else
                "<#{ @x }, #{ @y }, #{ @z } <=> #{ @u }, #{ @v }>"
            end
        end

        def -(v)
            has_no_uv = (not v.is_a? Vertex or @u.nil? or v.u.nil? or @v.nil? or v.v.nil?)
            Vertex.new (@x - v.x), (@y - v.y), (@z - v.z), (has_no_uv ? nil : (@u - v.u)), (has_no_uv ? nil : (@v - v.v))
        end

        def +(v)
            has_no_uv = (not v.is_a? Vertex or @u.nil? or v.u.nil? or @v.nil? or v.v.nil?)
            Vertex.new (@x + v.x), (@y + v.y), (@z + v.z), (has_no_uv ? nil : (@u + v.u)), (has_no_uv ? nil : (@v + v.v))
        end

        def *(factor)
            x, y, z = @x * factor, @y * factor, @z * factor

            if u.nil? or v.nil?
                u, v = nil, nil
            else
                u, v = @u * factor, @v * factor
            end

            Vertex.new x, y, z, u, v, @index
        end

        def map_near_two_others(a, b)
            c = self.dup
            r1, r2 = a.distance_to(c), b.distance_to(c)
            u1, v1 = a.u, a.v
            u2, v2 = b.u, b.v

            return [] if a.unmapped? or b.unmapped?

            # now, we have two projected vertices - `a` and `b` - taken from `e is an edge, projected already`
            # so, all we need - is to find the (u, v) coordinate pair for that vertex, `c`
            #
            # taken, A(x1, y1, z1) <=> A(u1, v1); B(x2, y2, z2) <=> B(u2, v2); C(x3, y3, z3) <=> C(u, v)
            # and BC = b1; AC = a1; we have these:
            d = a.distance_to(b)
            e = 0.25 * Math::sqrt( (d + r1 + r2) * (d + r1 - r2) * (d - r1 + r2) * (-d + r1 + r2) )

            res_uv = [
                [
                    ((u1 + u2) / 2.0) + (((u2 - u1) * ((r1**2.0) - (r2**2.0))) / (2.0 * (d**2.0))) + ((2.0 * (v1 - v2) * e) / (d**2.0)),

                    ((v1 + v2) / 2.0) + (((v2 - v1) * ((r1**2.0) - (r2**2.0))) / (2.0 * (d**2.0))) - ((2.0 * (u1 - u2) * e) / (d**2.0))
                ],
                [
                    ((u1 + u2) / 2.0) + (((u2 - u1) * ((r1**2.0) - (r2**2.0))) / (2.0 * (d**2.0))) - ((2.0 * (v1 - v2) * e) / (d**2.0)),

                    ((v1 + v2) / 2.0) + (((v2 - v1) * ((r1**2.0) - (r2**2.0))) / (2.0 * (d**2.0))) + ((2.0 * (u1 - u2) * e) / (d**2.0))
                ]
            ]

            res_uv.reject! do |uv|
                (!uv[0].is_a? Fixnum and uv[0].nan?) or (uv[0].infinite?) or
                (!uv[1].is_a? Fixnum and uv[1].nan?) or (uv[1].infinite?)
            end

            res_uv
        end

        def ==(v)
            if @index.nil?
                (v - self).length =~ Numeric::ETHA
            else
                @index == v.index
            end
        end

        def !=(v)
            not (self == v)
        end

        def distance_to_uv(v)
            Vector.new(@u, @v).distance_to Vector.new(v.u, v.v)
        end

        def unmapped?
            (@u.nil? or @v.nil?)
        end

        def mapped?
            not (@u.nil? or @v.nil?)
        end

        def copy_to
            c = self.dup
            c.index, c.copy_of = nil, self.index
            c.u, c.v = nil, nil

            c
        end

        def copy_pos_from(v)
            @x, @y, @z = v.x, v.y, v.z
        end

        def copy_uv_from(v)
            @u, @v = v.u, v.v
        end
    end

    class Edge < Line
        def reverse
            Edge.new @vertices.reverse
        end

        def in_segment_uv(v)
            if (@vertices[0].u != @vertices[1].u)
                # line is not  vertical
                if (@vertices[0].u <= v.u && v.u <= @vertices[1].u)
                    return true
                end

                if (@vertices[0].u >= v.u && v.u >= @vertices[1].u)
                    return true
                end
            else
                # S is vertical, so test y  coordinate
                if (@vertices[0].v <= v.v && v.v <= @vertices[1].v)
                    return true
                end

                if (@vertices[0].v >= v.v && v.v >= @vertices[1].v)
                    return true
                end
            end

            false
        end

        def intersect_uv_points(l, include_corners = false)
            # perp product  (2D)
            def perp(u, v)
                ((u.u * v.v) - (u.v * v.u)).to_f
            end

            # dot product (3D) which allows vector operations in arguments
            def dot(u, v)
                ((u.u * v.u) + (u.v * v.v)).to_f
            end

            filter_results = Proc.new do |res|
                if include_corners
                    [ res.size ] + res
                else
                    f = res.reject do |i|
                        not (self.vertices.select { |v| v.u =~ i.u and v.v =~ i.v }).empty? or
                        not (l.vertices.select { |v| v.u =~ i.u and v.v =~ i.v }).empty?
                    end

                    [ f.size ] + f
                end
            end

            u = @vertices[1] - @vertices[0]
            v = l.vertices[1] - l.vertices[0]
            w = @vertices[0] - l.vertices[0]
            d = perp(u, v)

            # test if  they are parallel (includes either being a point)
            if (d.abs < Numeric::ETHA) # S1 and S2 are parallel
                if (perp(u, w) != 0 || perp(v, w) != 0)
                    return [0] # they are NOT collinear
                end

                # they are collinear or degenerate
                # check if they are degenerate  points
                du = dot(u, u)
                dv = dot(v, v)

                if (du == 0 && dv == 0) # both segments are points
                    if (@vertices[0] !=  l.vertices[0]) # they are distinct  points
                        return [0]
                    end

                    i0 = @vertices[0] # they are the same point

                    return filter_results.call [i0]
                end

                if (du == 0) # S1 is a single point
                    if (l.in_segment_uv(@vertices[0]) == 0) # but is not in S2
                        return 0
                    end

                    i0 = @vertices[0]

                    return filter_results.call [i0]
                end

                if (dv == 0) # S2 a single point
                    if (self.in_segment_uv(l.vertices[0]) == 0) # but is not in S1
                        return [0]
                    end

                    i0 = l.vertices[0]

                    return filter_results.call [i0]
                end

                # they are collinear segments - get  overlap (or not)
                t0, t1 = nil, nil # endpoints of S1 in eqn for S2
                w2 = @vertices[1] - l.vertices[0]

                if (v.u != 0)
                    t0 = w.u / v.u
                    t1 = w2.u / v.u
                else
                    t0 = w.v / v.v
                    t1 = w2.v / v.v
                end

                if (t0 > t1) # must have t0 smaller than t1
                    t0, t1 = t1, t0 # swap if not
                end

                if (t0 > 1 || t1 < 0)
                    return [0] # NO overlap
                end

                t0 = (t0 < 0) ? 0 : t0 # clip to min 0
                t1 = (t1 > 1) ? 1 : t1 # clip to max 1

                if (t0 == t1) # intersect is a point
                    i0 = l.vertices[0] +  (v * t0)

                    return filter_results.call [i0]
                end

                # they overlap in a valid subsegment
                i0 = l.vertices[0] + (v * t0)
                i1 = l.vertices[0] + (v * t1)

                return filter_results.call [i0, i1]
            end

            # the segments are skew and may intersect in a point
            # get the intersect parameter for S1
            sI = perp(v, w) / d

            if (sI < 0 || sI > 1) # no intersect with S1
                return [0]
            end

            # get the intersect parameter for S2
            tI = perp(u, w) / d

            if (tI < 0 || tI > 1) # no intersect with S2
                return [0]
            end

            i0 = @vertices[0] + (u * sI) # compute S1 intersect point

            filter_results.call [i0]
        end

        def intersect_uv?(l)
            intersections = intersect_uv_points(l)

            intersections[0] == 1 and
            intersections[1] != l[0] and intersections[1] != l[1] and
            intersections[1] != self[0] and intersections[1] != self[1]
        end

        def by_same_side(v1, v2)
            # function SameSide(p1,p2, a,b)
            #     cp1 = CrossProduct(b-a, p1-a)
            #     cp2 = CrossProduct(b-a, p2-a)
            #     if DotProduct(cp1, cp2) >= 0 then return true
            #     else return false

            cp1 = (@vertices[1] - @vertices[0]).cross_product(v1 - @vertices[0])
            cp2 = (@vertices[1] - @vertices[0]).cross_product(v2 - @vertices[0])

            (cp1.dot_product(cp2) >= 0)
        end

        def by_same_uv_side(v1, v2)
            # function SameSide(p1,p2, a,b)
            #     cp1 = CrossProduct(b-a, p1-a)
            #     cp2 = CrossProduct(b-a, p2-a)
            #     if DotProduct(cp1, cp2) >= 0 then return true
            #     else return false

            p1 = Vector.new v1.u, v1.v
            p2 = Vector.new v2.u, v2.v
            a = Vector.new @vertices[0].u, @vertices[0].v
            b = Vector.new @vertices[1].u, @vertices[1].v

            cp1 = (b - a).cross_product(p1 - a)
            cp2 = (b - a).cross_product(p2 - a)

            (cp1.dot_product(cp2) >= 0)
        end

        alias_method :intersects_uv?, :intersect_uv?

        def to_uv_vector
            Vector.new(@vertices[1].u, @vertices[1].v) - Vector.new(@vertices[0].u, @vertices[0].v)
        end
    end

    class TriangleFace < Triangle
        attr_accessor :layer_index

        def sides
            a, b, c = @vertices[0], @vertices[1], @vertices[2]

            [ Edge.new(a, b), Edge.new(b, c), Edge.new(a, c) ]
        end

        def to_uv_s
            "<#{ (@vertices.map { |v| "(#{ v.u }, #{ v.v })" }).join '; ' }>"
        end

        def ==(triangle)
            raise "#{ triangle.inspect } is not a triangle!" unless triangle.is_a? Triangle

            triangle.vertices.permutation.to_a.each do |variant|
                return true if (Vector.new(variant[0].u, variant[0].v) == Vector.new(@vertices[0].u, @vertices[0].v)) and
                    (Vector.new(variant[1].u, variant[1].v) == Vector.new(@vertices[1].u, @vertices[1].v)) and
                    (Vector.new(variant[2].u, variant[2].v) == Vector.new(@vertices[2].u, @vertices[2].v))
            end

            ((@vertices.map { |v| v.index }) - (triangle.vertices.map { |v| v.index })).empty?
        end

        def area
            s = Vector.new
            n = @vertices.size

            0.upto(n - 1) do |i|
                j = (i + 1) % n

                s += @vertices[i].cross_product(@vertices[j])
            end

            (s / 2.0).length
        end

        def bbox
            min_x, min_y, max_x, max_y = nil, nil, nil, nil

            @vertices.each do |v|
                max_x = v.u if max_x.nil? or v.u >= max_x
                min_x = v.u if min_x.nil? or v.u <= min_x

                max_y = v.v if max_y.nil? or v.v >= max_y
                min_y = v.v if min_y.nil? or v.v <= min_y
            end

            [ min_x, max_x, min_y, max_y ]
        end

        def barycentric_coordinates(v)
            a = Vector.new @vertices[0].u, @vertices[0].v
            b = Vector.new @vertices[1].u, @vertices[1].v
            c = Vector.new @vertices[2].u, @vertices[2].v

            v0 = c - a
            v1 = b - a
            v2 = v - a

            # Compute dot products
            dot00 = v0.dot_product(v0)
            dot01 = v0.dot_product(v1)
            dot02 = v0.dot_product(v2)
            dot11 = v1.dot_product(v1)
            dot12 = v1.dot_product(v2)

            # Compute barycentric coordinates
            invDenom = 1.0 / ((dot00 * dot11) - (dot01 * dot01))
            u = ((dot11 * dot02) - (dot01 * dot12)) * invDenom
            v = ((dot00 * dot12) - (dot01 * dot02)) * invDenom

            { :u => u, :v => v }
        end

        def include_uv?(_v, strict = false)
            c = self.barycentric_coordinates(_v)
            u, v = c[:u], c[:v]

            # Check if point is in triangle
            if strict
                (u >= 0.0) && (v >= 0.0) && ((u + v) < 1.0)
            else
                (u > 0.0) && (v > 0.0) && ((u + v) < 1.0)
            end
        end

        def circumscribed_circle
            s = self.area
            r = ((self.sides.map { |s| s.to_uv_vector.length }).inject { |product, i| product * i }) / s

            ox = (1.0 / (4.0 * s)) * ([
                [ (self.vertices[0].u ** 2.0) + (self.vertices[0].v ** 2.0), self.vertices[0].v, 1.0 ],
                [ (self.vertices[1].u ** 2.0) + (self.vertices[1].v ** 2.0), self.vertices[1].v, 1.0 ],
                [ (self.vertices[2].u ** 2.0) + (self.vertices[2].v ** 2.0), self.vertices[2].v, 1.0 ]
            ]).det

            oy = (-1.0 / (4.0 * s)) * ([
                [ (self.vertices[0].u ** 2.0) + (self.vertices[0].v ** 2.0), self.vertices[0].u, 1.0 ],
                [ (self.vertices[1].u ** 2.0) + (self.vertices[1].v ** 2.0), self.vertices[1].u, 1.0 ],
                [ (self.vertices[2].u ** 2.0) + (self.vertices[2].v ** 2.0), self.vertices[2].u, 1.0 ]
            ]).det

            { :center => Vertex.new(0, 0, 0, ox, oy), :radius => r }
        end

        alias_method :includes_uv?, :include_uv?

        def overlaps_or_intersects_uv?(tri)
            raise "#{ tri.inspect } is not a triangle face!" unless tri.is_a? TriangleFace

            # compare distances to vertices
            vertex_combinations = @vertices.permutation(3).to_a

            are_equal = vertex_combinations.select do |vertex_set|
                vertex_set[0].index != @vertices[0].index and
                vertex_set[1].index != @vertices[1].index and
                vertex_set[2].index != @vertices[2].index and
                vertex_set[0].copy_of != @vertices[0].index and
                vertex_set[1].copy_of != @vertices[1].index and
                vertex_set[2].copy_of != @vertices[2].index and
                vertex_set[0].distance_to_uv(@vertices[0]) <= 1 and
                vertex_set[1].distance_to_uv(@vertices[1]) <= 1 and
                vertex_set[2].distance_to_uv(@vertices[2]) <= 1
            end

            return true unless are_equal.empty?

            # if intersection count > 1 and intersection points are not the vertex ones - return true
            side_intersections = (self.sides.product(tri.sides).map { |side_pair| side_pair[0].intersect_uv_points(side_pair[1]) })

            if (side_intersections.select { |i| i[0] > 0 }).size > 0
                return true
            end

            # if one triangle's vertex is within other triangle
            self.vertices.each do |vertex|
                c = tri.barycentric_coordinates(vertex)
                u, v = c[:u], c[:v]

                if u > 0.0 && v > 0.0 && (u + v) < 1.0
                    return true
                end
            end

            tri.vertices.each do |vertex|
                c = self.barycentric_coordinates(vertex)
                u, v = c[:u], c[:v]

                if u > 0.0 && v > 0.0 && (u + v) < 1.0
                    return true
                end
            end

            false
        end

        def uv_correct?
            (self.sides.select { |side| side[0].distance_to(side[1]) != side[0].distance_to_uv(side[1]) }).empty?
        end

        def find_conflicting_vertex
            @vertices.each_with_index do |v, i|
                a, b = @vertices - [ v ]

                if (v.distance_to(a) != v.distance_to_uv(a) or v.distance_to(b) != v.distance_to_uv(b))
                    return { :index => i, :vertex => v, :others => [a, b] }
                end
            end

            nil
        end

        def find_vertex_index(v)
            @vertices.each_with_index { |t, i| return i if t == v }

            nil
        end

        def find_others(v)
            @vertices.reject { |t| t == v }
        end
    end

    class Mesh
        attr_accessor :vertices, :indices, :triangles, :scale_factor

        def initialize(v, i)
            @vertices = v.each_with_index.map { |vertex, index| Vertex.new vertex.x, vertex.y, vertex.z, nil, nil, index }
            @indices = i.dup

            fit_size!
        end

        def fit_size!(fitness_size = 100)
            tris = @indices.each_slice(3).to_a.map { |t| TriangleFace.new @vertices[t[0]], @vertices[t[1]], @vertices[t[2]] }
            scale_factor = 1.0

            tris.each do |t|
                t.sides.each do |s|
                    l = s.to_vector.length
                    l = 1.0 if l =~ 0.0
                    f = fitness_size.to_f / l.to_f
                    f = fitness_size if f <= 1.0

                    if l < fitness_size and f > scale_factor
                        scale_factor = f
                    end
                end
            end

            scale_factor = scale_factor.ceil

            if scale_factor > 1.0
                @vertices.map! { |v| v * scale_factor }
            end
        end

        def find_layer_to_fit(triangle)
            layer_index = -1

            @layers.each_with_index do |layer, l_i|
                layer_found = true

                layer.each do |overlap_candidate|
                    if triangle == overlap_candidate or overlap_candidate.overlaps_or_intersects_uv?(triangle)
                        # puts "!!!!! layer triangle #{ overlap_candidate } overlaps or intersects mapped candidate #{ triangle }"
                        layer_found = false
                        break
                    end
                end

                if layer_found
                    layer_index = l_i
                    break
                end
            end

            layer_index
        end

        def find_parents_for_vertex(v)
            results = []

            @triangles.select do |triangle|
                vertex_indexes = ((triangle.vertices.map { |v1| v1.index }) + (triangle.vertices.map { |v1| v1.copy_of })).compact

                if (vertex_indexes.include? v.index) or (vertex_indexes.include? v.copy_of)
                    results << triangle
                end
            end

            results
        end

        def map_triangle(triangle_index)
            parent_triangle = @triangles[triangle_index]

            # if our mapped vertices form an invalid side - clone them to virtuals!
            [0,1,2].combination(2).to_a.each do |side|
                a, b = parent_triangle.vertices[side[0]], parent_triangle.vertices[side[1]]

                if a.mapped? and b.mapped? and not (a.distance_to(b) =~ a.distance_to_uv(b))
                    a_index, b_index = side[0], side[1]

                    a, b = a.copy_to, b.copy_to
                    a.u, a.v = nil, nil
                    b.u, b.v = nil, nil
                    a.index, b.index = @vertices.size, @vertices.size + 1
                    @vertices += [a, b]

                    parent_triangle[a_index], parent_triangle[b_index] = a, b

                    @triangles[triangle_index] = parent_triangle
                end
            end

            unmapped_vertices = (parent_triangle.vertices.select { |v| not v.mapped? })

            # if triangle vertices are mapped already - check if we need to create some virtual vertices
            if unmapped_vertices.empty?
                if parent_triangle.uv_correct?
                    @mapped_triangles_indexes[triangle_index] = true
                    @latest_mapped[triangle_index] = true
                else
                    # create virtual triangle
                    conflict = parent_triangle.find_conflicting_vertex

                    a_origin, b_origin = conflict[:others]

                    c = conflict[:vertex].copy_to

                    # try to map conflicting vertex near one of its parents
                    parents = find_parents_for_vertex(conflict[:vertex])

                    parents.each do |candidate|
                        break if @mapped_triangles_indexes[triangle_index]

                        a, b = candidate.find_others(conflict[:vertex])

                        # could not map near differently sized edge
                        next if a.distance_to(b) != a_origin.distance_to(b_origin)

                        res_uv = c.map_near_two_others(a, b)

                        res_uv.each do |uv|
                            tmp_c = c.copy_to
                            tmp_c.u, tmp_c.v = uv[0], uv[1]

                            tmp_triangle = TriangleFace.new(a, b, tmp_c)

                            next unless tmp_triangle.uv_correct?

                            layer_index = find_layer_to_fit(tmp_triangle)

                            # map if we can
                            if layer_index > -1
                                tmp_c.index = @vertices.size
                                @vertices << tmp_c

                                parent_triangle = tmp_triangle
                                parent_triangle.layer_index = layer_index

                                @layers[layer_index] << parent_triangle

                                @triangles[triangle_index] = parent_triangle

                                @mapped_triangles_indexes[triangle_index] = true
                                @latest_mapped[triangle_index] = true

                                break
                            end
                        end
                    end

                    # if we did not find any layer to fit mapping - map onto new plane
                    unless @mapped_triangles_indexes[triangle_index]
                        a, b, c = parent_triangle.vertices

                        a, b, c = a.copy_to, b.copy_to, c.copy_to

                        a.u, a.v = 0, 0
                        b.u, b.v = 0, b.distance_to(a)
                        c.u, c.v = c.map_near_two_others(a, b)[0]

                        a.index, b.index, c.index = @vertices.size, @vertices.size + 1, @vertices.size + 2

                        @vertices += [ a, b, c ]

                        parent_triangle.vertices = [a, b, c]

                        parent_triangle.layer_index = @layers.size

                        @layers << [ parent_triangle ]

                        @triangles[triangle_index] = parent_triangle

                        @mapped_triangles_indexes[triangle_index] = true
                        @latest_mapped[triangle_index] = true
                    end
                end
            # if only one vertex is not mapped - map it near other two or its clones
            elsif unmapped_vertices.size == 1
                c = unmapped_vertices.first
                c_index = parent_triangle.find_vertex_index(c)

                a_origin, b_origin = parent_triangle.find_others(c)

                parents = find_parents_for_vertex(c)

                parents.each do |candidate|
                    break if @mapped_triangles_indexes[triangle_index]

                    a, b = candidate.find_others(c)

                    next if a.distance_to(b) != a_origin.distance_to(b_origin) or a.distance_to_uv(b) != a_origin.distance_to_uv(b_origin)

                    res_uv = c.map_near_two_others(a, b)

                    res_uv.each do |uv|
                        tmp_c = c.dup
                        tmp_c.u, tmp_c.v = uv[0], uv[1]

                        tmp_triangle = TriangleFace.new(a, b, tmp_c)

                        layer_index = find_layer_to_fit(tmp_triangle)

                        # map if we can
                        if layer_index > -1
                            parent_triangle.vertices[c_index] = tmp_c
                            parent_triangle.layer_index = layer_index

                            @layers[layer_index] << parent_triangle

                            @triangles[triangle_index] = parent_triangle

                            @mapped_triangles_indexes[triangle_index] = true
                            @latest_mapped[triangle_index] = true

                            break
                        end
                    end
                end

                # if we did not find any layer to fit mapping - map onto new plane
                unless @mapped_triangles_indexes[triangle_index]
                    parent_triangle.vertices.map! { |v| v.copy_to }

                    a, b, c = parent_triangle.vertices

                    a.u, a.v = 0, 0
                    b.u, b.v = 0, b.distance_to(a)
                    c.u, c.v = c.map_near_two_others(a, b)[0]

                    a.index, b.index, c.index = @vertices.size, @vertices.size + 1, @vertices.size + 2

                    @vertices += [ a, b, c ]

                    parent_triangle.vertices = [a, b, c]

                    parent_triangle.layer_index = @layers.size

                    @layers << [ parent_triangle ]

                    @triangles[triangle_index] = parent_triangle

                    @mapped_triangles_indexes[triangle_index] = true
                    @latest_mapped[triangle_index] = true
                end
                # if two vertices are not mapped - find a combination which we could map and map
            elsif unmapped_vertices.size == 2
                a_origin = (parent_triangle.vertices - unmapped_vertices).first

                parents = find_parents_for_vertex(a_origin)

                # do not use self and triangles which do not have something in common with self
                parents.reject! { |p| ((p.vertices.map{|v| v.index} + p.vertices.map{|v| v.copy_of}).compact - (parent_triangle.vertices.map {|v| v.index} + parent_triangle.vertices.map {|v| v.copy_of}).compact).empty? }

                parents.each do |candidate|
                    candidate_v_indexes = ((candidate.vertices.map { |v| v.index }) + (candidate.vertices.map { |v| v.copy_of })).compact
                    parent_v_indexes = ((parent_triangle.vertices.map { |v| v.index }) + (parent_triangle.vertices.map { |v| v.copy_of })).compact
                    common_vertex_indexes = candidate_v_indexes & parent_v_indexes

                    next if common_vertex_indexes.size < 2

                    a, b = candidate.vertices.select { |v| common_vertex_indexes.include?(v.index) or common_vertex_indexes.include?(v.copy_of) }
                    c = (parent_triangle.vertices - [a, b]).first

                    res_uv = c.map_near_two_others(a, b)

                    res_uv.each do |uv|
                        tmp_c = c.dup
                        tmp_c.u, tmp_c.v = uv[0], uv[1]

                        tmp_triangle = TriangleFace.new(a, b, tmp_c)

                        layer_index = find_layer_to_fit(tmp_triangle)

                        # map if we can
                        if layer_index > -1
                            tmp_triangle.layer_index = layer_index

                            @layers[layer_index] << tmp_triangle

                            @triangles[triangle_index] = tmp_triangle

                            @mapped_triangles_indexes[triangle_index] = true
                            @latest_mapped[triangle_index] = true

                            break
                        end
                    end
                end
            # if three vertices are not mapped - try to map near some other or map a new triangle
            elsif unmapped_vertices.size == 3
                parents = []

                parent_triangle.vertices.combination(2).each do |pair|
                    tmp = @triangles.select do |p|
                        v_indexes = ((p.vertices.map { |v| v.index }) + (p.vertices.map { |v| v.copy_of })).compact
                        (v_indexes.include? pair[0].index) and (v_indexes.include? pair[1].index) and not (v_indexes.include? (parent_triangle.vertices - pair).first.index)
                    end

                    parents += tmp
                end

                parents.each do |candidate|
                    parent_v_indexes = (parent_triangle.vertices.map {|v2| v2.index})

                    a, b = candidate.vertices.select { |v| v.mapped? and (parent_v_indexes.include? v.index or parent_v_indexes.include? v.copy_of) }

                    next if a.nil? or b.nil?

                    c = (parent_triangle.vertices.select { |v| v.index != a.index and v.index != a.copy_of and v.index != b.index and v.index != b.copy_of }).first

                    res_uv = c.map_near_two_others(a, b)

                    res_uv.each do |uv|
                        tmp_c = c.dup
                        tmp_c.u, tmp_c.v = uv[0], uv[1]

                        tmp_triangle = TriangleFace.new(a, b, tmp_c)

                        layer_index = find_layer_to_fit(tmp_triangle)

                        # map if we can
                        if layer_index > -1
                            tmp_triangle.layer_index = layer_index

                            @layers[layer_index] << tmp_triangle

                            @triangles[triangle_index] = tmp_triangle

                            @mapped_triangles_indexes[triangle_index] = true
                            @latest_mapped[triangle_index] = true

                            break
                        end
                    end
                end

                unless @mapped_triangles_indexes[triangle_index]
                    a, b, c = parent_triangle.vertices

                    a.u, a.v = 0, 0
                    b.u, b.v = 0, b.distance_to(a)
                    c.u, c.v = c.map_near_two_others(a, b)[0]

                    parent_triangle.vertices = [a, b, c]

                    parent_triangle.layer_index = @layers.size

                    @layers << [ parent_triangle ]

                    @triangles[triangle_index] = parent_triangle

                    @mapped_triangles_indexes[triangle_index] = true
                    @latest_mapped[triangle_index] = true
                end
            end
        end

        def unwrap_to(texture_filename = 'result.png')
            @layers = []

            @triangles = @indices.each_slice(3).to_a

            t = @triangles.size

            # set the list of mapped already triangles to empty one
            @mapped_triangles_indexes = Hash[(0 .. t - 1).to_a.map { |k| [k, false] }]
            @latest_mapped = Hash[(0 .. t - 1).to_a.map { |k| [k, false] }]

            @connected_triangles = Hash[(0 .. t - 1).to_a.map {|i| [i, []]}]

            # create a list of connected triangles
            # e. g. connected_triangles[7] = [ 1, 2, 7 ] if triangle #7 has neighbours 1, 2 and 7
            @triangles.each_with_index do |tri1, tri1_index|
                @triangles.each_with_index do |tri2, tri2_index|
                    next if (tri1_index == tri2_index)

                    if (tri1 & tri2).size >= 2
                        @connected_triangles[tri1_index] << tri2_index
                    end
                end
            end

            @triangles.map! do |triangle_vertex_indices|
                TriangleFace.new @vertices[triangle_vertex_indices[0]], @vertices[triangle_vertex_indices[1]], @vertices[triangle_vertex_indices[2]]
            end

            # puts " >> Connected list: #{ @connected_triangles.inspect }"

            # if connected_triangles[i] > 3 then some pair of vertices (edge) belongs to
            # 3 or more triangles which is incorrect. so we should clone those vertices
            # and create a new separated triangles
            #connected_triangles.each_index do |tri1_index|
            #    if connected_triangles[tri1_index].size > 3
            #        pass
            #    end
            #end

            # set the list of mapped already triangles to empty one
            @mapped_triangles_indexes = Hash[(0 .. t - 1).to_a.map { |k| [k, false] }]
            @last_mapped_indexes = Hash[(0 .. t - 1).to_a.map { |k| [k, false] }]

            begin
                # find the triangle that was already mapped or take the first unmapped one
                candidates_to_be_mapped = @mapped_triangles_indexes.select { |k, v| v and (not @last_mapped_indexes[k]) }

                if candidates_to_be_mapped.empty?
                    # take the first unmapped
                    parent_triangle_index, triangle_neighbours = @connected_triangles.shift
                else
                    # take the first mapped one
                    parent_triangle_index = candidates_to_be_mapped.first.first
                    triangle_neighbours = @connected_triangles.delete(parent_triangle_index)
                end

                next if triangle_neighbours.nil?

                @last_mapped_indexes[parent_triangle_index] = true

                # if triangle is not mapped yet - map it!
                unless @mapped_triangles_indexes[parent_triangle_index]
                    map_triangle(parent_triangle_index)

                    @mapped_triangles_indexes[parent_triangle_index] = true

                    if $DEBUG_DRAW
                        TextureRenderer::render_layers(@layers, texture_filename, parent_triangle_index)
                        puts "> Drawn triangle ##{ parent_triangle_index }. Press Enter to continue..."
                        gets
                    end
                end

                # now, map all its neighbours near our triangle
                triangle_neighbours.each do |neighbour_triangle_index|
                    # do not map what was already mapped
                    next if @mapped_triangles_indexes[neighbour_triangle_index]

                    map_triangle(neighbour_triangle_index)

                    # mark the neighbour as already mapped one
                    @mapped_triangles_indexes[neighbour_triangle_index] = true

                    if $DEBUG_DRAW
                        TextureRenderer::render_layers(@layers, texture_filename, neighbour_triangle_index)
                        puts ">> Drawn triangle ##{ neighbour_triangle_index }. Press Enter to continue..."
                        #puts "Drawn already: #{ @mapped_triangles_indexes }"
                        gets
                    end
                end
            end while @connected_triangles.size > 0 and (@mapped_triangles_indexes.select { |k, v| not v }).size > 0


            puts "#{ DateTime.now } > Unwrapping done. Writing unwrapped texture to #{ texture_filename }..."

            TextureRenderer::render_layers(@layers, texture_filename)

            puts "#{ DateTime.now } > Generated layers count: #{ @layers.size }"

            @layers
        end
    end

    class TextureRenderer
        def self.render_layers(layers, filename, highlight_triangle = nil)
            # default current layer offset value
            off_x, off_y = 0, 0
            width, height = 0, 0

            layers.each_with_index do |layer, layer_index|
                # create a new graphics context
                gc = Magick::Draw.new

                # puts ">>> LAYER #{ layer_index } --> #{ layer.map { |t| t.to_uv_s } }"
                # find BBox for each layer
                min_x, max_x, min_y, max_y = nil, nil, nil, nil

                layer.each do |triangle|
                    u1, u2, v1, v2 = triangle.bbox

                    min_x = u1 if min_x.nil? or u1 <= min_x
                    max_x = u2 if max_x.nil? or u2 >= max_x

                    min_y = v1 if min_y.nil? or v1 <= min_y
                    max_y = v2 if max_y.nil? or v2 >= max_y
                end

                delta_u = off_x + min_x.abs
                delta_v = off_y + min_y.abs

                # and draw a new polygon set in a graphics context
                layer.each_with_index do |face, face_index|
                    if not highlight_triangle.nil? and face_index == highlight_triangle
                        gc.stroke('#FF0000')
                        gc.stroke_width(4)
                    else
                        gc.stroke('#8A0808')
                        gc.stroke_width(2)
                    end

                    gc.fill('#0040FF30')

                    # NOTE: drawing with the min_u and min_v offset
                    # to prevent negative coordinates and with
                    # off_x and off_y (current layer offset) to
                    # compact the resulting image as much as possible
                    points = (face.vertices.map { |v| [ delta_u + v.u, delta_v + v.v ] }).flatten

                    # puts ">>> DRAWING: #{ points }"

                    gc.polygon(*points)
                end

                # increase the layer offsets by the current layer dimensions
                width = max_x.abs + min_x.abs
                height = max_y.abs + min_y.abs

                #off_x += width
                #off_y += height

                if !width.is_a? Numeric or width < 1.0 or (!width.is_a? Fixnum and width.nan?)
                    next
                end

                if !height.is_a? Numeric or height < 1.0 or (!height.is_a? Fixnum and height.nan?)
                    next
                end

                # and finally, draw the layer composition to the image
                # NOTE: the second argument for HatchFill sets the grid color
                canvas = Magick::Image.new(width, height, Magick::HatchFill.new('white', 'lightcyan2'))
                gc.draw(canvas)
                canvas.write("#{ filename.gsub(File.extname(filename), '') }_#{ layer_index }#{ File.extname(filename) }")
            end

            # and finally, draw the layer composition to the image
            # NOTE: the second argument for HatchFill sets the grid color
            # canvas = Magick::Image.new(width, height, Magick::HatchFill.new('white', 'lightcyan2'))
            # gc.draw(canvas)
            # canvas.write(filename)
        end
    end
end

include MooMesh
