$LOAD_PATH.unshift File.dirname(__FILE__)

require 'moo_mesh'
require 'moo_obj_file'

# o = ObjFile.open('cube.obj')
# m = o.to_mesh
# m.unwrap_to('new_cube.png')

# scale = 1.0

# cube = Mesh.new(
#     [
#         Vector.new(0.0,  0.0,  0.0) * scale, # 0
#         Vector.new(0.0,  0.0,  1.0) * scale, # 1
#         Vector.new(0.0,  1.0,  0.0) * scale, # 2
#         Vector.new(0.0,  1.0,  1.0) * scale, # 3
#         Vector.new(1.0,  0.0,  0.0) * scale, # 4
#         Vector.new(1.0,  0.0,  1.0) * scale, # 5
#         Vector.new(1.0,  1.0,  0.0) * scale, # 6
#         Vector.new(1.0,  1.0,  1.0) * scale  # 7
#     ],
#     [
#         0, 6, 4,
#         2, 3, 7,

#         0, 3, 2,
#         0, 1, 3,

#         2, 7, 6,
#         0, 2, 6,

#         4, 6, 7,
#         4, 7, 5,

#         0, 4, 5,
#         0, 5, 1,

#         1, 5, 7,
#         1, 7, 3
#     ]
# )

# cube.unwrap_to('old_cube.png')

o = ObjFile.open('hammer.obj')
m = o.to_mesh
m.unwrap_to('hammer.png')

# o = ObjFile.open('hammer_triangulated.obj')
# m = o.to_mesh
# m.unwrap_to('hammer_triangulated.png')

# o = ObjFile.open('qt.obj')
# m = o.to_mesh
# m.unwrap_to('qt.png')
